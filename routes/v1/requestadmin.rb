# frozen_string_literal: true

# Functions for working with user requests
class Manager2Api < Sinatra::Base
  # Delete a pending request
  post '/v1/request/delete' do
    check_key_admin(params)
    check_param(params, 'id')

    halt erb :error, locals: { code: 400, message: 'Invalid request ID' } if params['id'].to_i.zero? && params['id'] != '0'

    halt erb :error, locals: { code: 404, message: 'Request ID out of range' } unless request_delete(params['id'].to_i)

    log_info "Deleted request: #{params['id']}"
    erb :result, locals: { message: 'Request deleted' }
  end

  # List all pending requests
  post '/v1/request/list' do
    check_key_admin(params)
    erb :result, locals: { message: 'Retreived pending requests', result: requests_get }
  end

  # Send notification via IRC
  post '/v1/notify/irc' do
    check_key_admin(params)
    check_params(params, %w[username message])
    notify_irc(params['message'], channels: [params['username']])
    erb :result, locals: { message: 'Message sent' }
  end

  # Send notification via email
  post '/v1/notify/email' do
    check_key_admin(params)
    check_params(params, %w[username subject body])

    notify_mail_user(
      params['username'],
      params['subject'],
      params['body']
    )
    erb :result, locals: { message: 'Email sent' }
  end

  # Send email with request approved mail template
  post '/v1/notify/email/approved' do
    check_key_admin(params)
    check_params(params, %w[username details])
    notify_mail_user(
      params['username'],
      'Your request has been approved',
      (erb :'email/request_approved', locals: {
        username: params['username'],
        details: params['details']
      })
    )
    erb :result, locals: { message: 'Email sent' }
  end

  # Send email with request rejected mail template
  post '/v1/notify/email/rejected' do
    check_key_admin(params)
    check_params(params, %w[username details comments])
    notify_mail_user(
      params['username'],
      'Your request has been rejected',
      (erb :'email/request_rejected', locals: {
        username: params['username'],
        details: params['details'],
        comments: params['comments']
      })
    )
    erb :result, locals: { message: 'Email sent' }
  end
end
