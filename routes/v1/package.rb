# frozen_string_literal: true

# Functions for working with packages
class Manager2Api < Sinatra::Base
  # Request package install
  post '/v1/package/request' do
    check_key(params)
    check_param(params, 'package')

    data    = git_get_full
    user    = git_user_from_key(params['key'])
    comment = params.key?('comment') ? params['comment'] : ''

    # Check if package is already on list
    if data['profile::base::packages'].include?(params['package']) ||
       data['profile::deb_backports'].include?(params['package'])
      halt erb :error, locals: { code: 409, message: 'Package is already installed' }
    end

    options = { 'package' => params['package'], 'comment' => comment }
    options['backports'] = true if params.key?('backports') && (params['backports'] == 'true')

    request_submit(user, options)

    erb :result, locals: { message: 'Package install request received' }
  end

  # Install a package
  post '/v1/package/new' do
    check_key_admin(params)
    check_param(params, 'package')

    data = git_get_full

    # Check if package is already on list
    if data['profile::base::packages'].include?(params['package']) ||
       data['profile::deb_backports'].include?(params['package'])
      halt erb :error, locals: { code: 409, message: 'Package is already installed' }
    end

    list = 'profile::base::packages'
    list = 'profile::deb_backports' if params.key?('backports') && (params['backports'] == 'true')
    # Add package to array and sort
    data[list].push params['package']
    data[list].sort!
    git_put_full(data, "package: #{params['package']}")

    erb :result, locals: { message: 'Package will be installed' }
  end
end
