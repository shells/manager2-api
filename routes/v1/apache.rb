# frozen_string_literal: true

# rubocop:disable Metrics/ClassLength
# Functions for working with apache vhosts
class Manager2Api < Sinatra::Base
  # List configured vhosts
  post '/v1/apache/vhost/list' do
    check_key(params)
    data   = git_get
    user   = git_user_from_key(params['key'])
    result = []

    result.push('apache_vhosts' => data[user]['properties']['apache_vhosts']) if data[user]['properties'].key? 'apache_vhosts'

    result.push('apache_proxies' => data[user]['properties']['apache_proxies']) if data[user]['properties'].key? 'apache_proxies'

    erb :result, locals: { message: 'Retreived configured vhosts and proxies', result: result }
  end

  # Request a new vhost
  post '/v1/apache/vhost/request' do
    check_key(params)
    check_param(params, 'domain')

    data    = git_get
    user    = git_user_from_key(params['key'])
    comment = params.key?('comment') ? params['comment'] : ''
    options = {}

    # Checks if vhost exists
    if data[user]['properties'].key?('apache_vhosts') && (data[user]['properties']['apache_vhosts'].key? params['domain'])
      halt erb :error, locals: { code: 409, message: 'Vhost with the requested domain already exists' }
    end

    # Process additional parameters
    if params.key? 'documentroot'
      unless params['documentroot'].match?(%r{^/home/#{user}/})
        halt erb :error, locals: { code: 400, message: 'Documentroot cannot be outside of users home' }
      end
      options['documentroot'] = params['documentroot']
    end

    options['strongtls'] = true if params.key?('strongtls') && (params['strongtls'] == 'true')

    # Submit request
    request_submit(user, 'domain' => params['domain'], 'options' => options, 'comment' => comment)

    erb :result, locals: { message: 'Vhost request received' }
  end

  # Remove a vhost
  post '/v1/apache/vhost/remove' do
    check_key(params)
    check_param(params, 'domain')

    data = git_get
    user = git_user_from_key(params['key'])

    # Find and update vhosts
    vhost_found = false
    if data[user]['properties'].key?('apache_vhosts') && data[user]['properties']['apache_vhosts'].key?(params['domain'])
      data[user]['properties']['apache_vhosts'][params['domain']]['removed'] = true
      data[user]['properties']['apache_vhosts'][params['domain']].delete 'disabled'
      vhost_found = true
    elsif data[user]['properties'].key?('apache_proxies') && data[user]['properties']['apache_proxies'].key?(params['domain'])
      data[user]['properties']['apache_proxies'][params['domain']]['removed'] = true
      data[user]['properties']['apache_proxies'][params['domain']].delete 'disabled'
      vhost_found = true
    end

    # Checks if vhost was found
    halt erb :error, locals: { code: 404, message: 'Vhost with the requested domain was not found' } unless vhost_found

    # Update config
    git_put data, "vhost: #{params['domain']}, user: #{user}"
    erb :result, locals: { message: 'The vhost is removed' }
  end

  # Disable a vhost
  post '/v1/apache/vhost/disable' do
    check_key(params)
    check_param(params, 'domain')

    data = git_get
    user = git_user_from_key(params['key'])

    # Find and update vhosts
    vhost_found = false
    if data[user]['properties'].key?('apache_vhosts') && data[user]['properties']['apache_vhosts'].key?(params['domain'])
      data[user]['properties']['apache_vhosts'][params['domain']]['disabled'] = true
      data[user]['properties']['apache_vhosts'][params['domain']].delete 'removed'
      vhost_found = true
    elsif data[user]['properties'].key?('apache_proxies') && data[user]['properties']['apache_proxies'].key?(params['domain'])
      data[user]['properties']['apache_proxies'][params['domain']]['disabled'] = true
      data[user]['properties']['apache_proxies'][params['domain']].delete 'removed'
      vhost_found = true
    end

    # Checks if vhost was found
    halt erb :error, locals: { code: 404, message: 'Vhost with the requested domain was not found' } unless vhost_found

    # Update config
    git_put data, "vhost: #{params['domain']}, user: #{user}"
    erb :result, locals: { message: 'The vhost is disabled' }
  end

  # Enable a vhost
  post '/v1/apache/vhost/enable' do
    check_key(params)
    check_param(params, 'domain')

    data = git_get
    user = git_user_from_key(params['key'])

    # Find and update vhosts
    vhost_found = false
    if data[user]['properties'].key?('apache_vhosts') && data[user]['properties']['apache_vhosts'].key?(params['domain'])
      data[user]['properties']['apache_vhosts'][params['domain']].delete 'removed'
      data[user]['properties']['apache_vhosts'][params['domain']].delete 'disabled'
      vhost_found = true
    elsif data[user]['properties'].key?('apache_proxies') && data[user]['properties']['apache_proxies'].key?(params['domain'])
      data[user]['properties']['apache_proxies'][params['domain']].delete 'removed'
      data[user]['properties']['apache_proxies'][params['domain']].delete 'disabled'
      vhost_found = true
    end

    # Checks if vhost was found
    halt erb :error, locals: { code: 404, message: 'Vhost with the requested domain was not found' } unless vhost_found

    # Update config
    git_put data, "vhost: #{params['domain']}, user: #{user}"
    erb :result, locals: { message: 'The vhost is enabled' }
  end

  # rubocop:disable Metrics/BlockLength
  # Create a new vhost
  post '/v1/apache/vhost/new' do
    check_key_admin(params)
    check_params(params, %w[domain username])

    data = git_get

    # Check if the user exists
    halt erb :error, locals: { code: 404, message: 'The user cannot be found' } unless data.key? params['username']

    # Create apache_vhosts config hash if it does not exist already
    data[params['username']]['properties']['apache_vhosts'] = {} unless data[params['username']]['properties'].key? 'apache_vhosts'

    # Check for existing domain
    if data[params['username']]['properties']['apache_vhosts'].key? params['domain']
      halt erb :error, locals: { code: 409, message: 'The vhost you are trying to create already exists' }
    end

    # Build new vhost config
    data[params['username']]['properties']['apache_vhosts'][params['domain']] = {}

    # rubocop:disable Style/RedundantLineContinuation
    unless params['domain'].match?(/\.insomnia247\.nl$/)
      data[params['username']]['properties']['apache_vhosts'] \
      [params['domain']]['customdomain'] = true
    end

    if params.key?('strongtls') && (params['strongtls'] == 'true')
      data[params['username']]['properties']['apache_vhosts'] \
      [params['domain']]['strongtls'] = true
    end

    if params.key? 'documentroot'
      unless params['documentroot'].start_with?("/home/#{params['username']}/")
        halt erb :error, locals: { code: 400, message: 'Documentroot cannot be outside of users home' }
      end
      data[params['username']]['properties']['apache_vhosts'] \
      [params['domain']]['documentroot'] = params['documentroot']
    end
    # rubocop:enable Style/RedundantLineContinuation

    git_put data, "domain: #{params['domain']}, user: #{params['username']}"

    erb :result, locals: { message: 'The vhost is created' }
  end
  # rubocop:enable Metrics/BlockLength
end
# rubocop:enable Metrics/ClassLength
