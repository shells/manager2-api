# frozen_string_literal: true

# Functions for working with limits and quotas
class Manager2Api < Sinatra::Base
  # List configured limites
  post '/v1/limit/list' do
    check_key(params)
    user    = git_user_from_key(params['key'])
    results = {
      cpu: limit_cpu_get(user),
      memory: limit_memory_get(user),
      disk: limit_disk_get(user)
    }
    erb :result, locals: { message: 'Retreived limits', result: results }
  end

  # Request new disk quota
  post '/v1/limit/disk/request' do
    check_key(params)
    check_param(params, 'limit')

    halt erb :error, locals: { code: 400, message: 'Provided value for limit does not look like a number' } unless params['limit'].match?(/^[0-9]+$/)

    user    = git_user_from_key(params['key'])
    comment = params.key?('comment') ? params['comment'] : ''

    request_submit(user, 'limit' => params['limit'], 'comment' => comment)
    erb :result, locals: { message: 'Request received' }
  end

  # Set a users disk quota
  post '/v1/limit/disk/set' do
    check_key_admin(params)
    check_params(params, %w[username limit])

    halt erb :error, locals: { code: 400, message: 'Provided value for limit does not look like a number' } unless params['limit'].match?(/^[0-9]+$/)

    limit_disk_set params['username'], params['limit']

    erb :result, locals: { message: 'Diskspace limit configured' }
  end

  # Get a users current disk quota
  post '/v1/limit/disk/get' do
    check_key_admin(params)
    check_param(params, 'username')

    erb :result, locals: { message: 'Retreived limit', result: limit_disk_get(params['username']) }
  end

  # Request higher process count limit
  post '/v1/limit/process/request' do
    check_key(params)
    user    = git_user_from_key(params['key'])
    comment = params.key?('comment') ? params['comment'] : ''

    request_submit(user, 'comment' => comment)
    erb :result, locals: { message: 'Request received' }
  end

  # Set higher process count limit
  post '/v1/limit/process/set' do
    check_key_admin(params)
    check_param(params, 'username')

    ldap_add_user_to_group params['username'], 'raised-proc'

    erb :result, locals: { message: 'Raised process limit configured' }
  end

  # Request new cpu shares cap
  post '/v1/limit/cpu/request' do
    check_key(params)
    check_param(params, 'limit')

    halt erb :error, locals: { code: 400, message: 'Provided value for limit does not look like a number' } unless params['limit'].match?(/^[0-9]+$/)

    user    = git_user_from_key(params['key'])
    comment = params.key?('comment') ? params['comment'] : ''

    request_submit(user, 'limit' => params['limit'], 'comment' => comment)
    erb :result, locals: { message: 'Request received' }
  end

  # Set a users cpu shares cap
  post '/v1/limit/cpu/set' do
    check_key_admin(params)
    check_params(params, %w[username limit])

    halt erb :error, locals: { code: 400, message: 'Provided value for limit does not look like a number' } unless params['limit'].match?(/^[0-9]+$/)

    limit_cpu_set params['username'], params['limit']

    erb :result, locals: { message: 'CPU shares limit configured' }
  end

  # Get a users cpu shares cap
  post '/v1/limit/cpu/get' do
    check_key_admin(params)
    check_param(params, 'username')

    erb :result, locals: { message: 'Retreived limit', result: limit_cpu_get(params['username']) }
  end

  # Request new memory usage cap
  post '/v1/limit/memory/request' do
    check_key(params)
    check_param(params, 'limit')

    halt erb :error, locals: { code: 400, message: 'Provided value for limit does not look like a number' } unless params['limit'].match?(/^[0-9]+$/)

    user    = git_user_from_key(params['key'])
    comment = params.key?('comment') ? params['comment'] : ''

    request_submit(user, 'limit' => params['limit'], 'comment' => comment)
    erb :result, locals: { message: 'Request received' }
  end

  # Set a users memory usage cap
  post '/v1/limit/memory/set' do
    check_key_admin(params)
    check_params(params, %w[username limit])

    halt erb :error, locals: { code: 400, message: 'Provided value for limit does not look like a number' } unless params['limit'].match?(/^[0-9]+$/)

    limit_memory_set params['username'], params['limit']

    erb :result, locals: { message: 'Memory limit configured' }
  end

  # Get a users memory usage cap
  post '/v1/limit/memory/get' do
    check_key_admin(params)
    check_param(params, 'username')

    erb :result, locals: { message: 'Retreived limit', result: limit_memory_get(params['username']) }
  end
end
