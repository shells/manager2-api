# frozen_string_literal: true

# Functions for working with MySQL databases
class Manager2Api < Sinatra::Base
  # List configured databases
  post '/v1/mysql/list' do
    check_key(params)

    data    = git_get
    user    = git_user_from_key(params['key'])
    results = []

    results = data[user]['properties']['mysql_dbs'].keys if data[user]['properties'].key? 'mysql_dbs'

    erb :result, locals: { message: 'Retreived MySQL databases', result: results }
  end

  # Request a database
  post '/v1/mysql/request' do
    check_key(params)
    check_param(params, 'dbname')

    # Check db name for bad chars
    halt erb :error, locals: { code: 400, message: 'Illegal character in database name' } unless params['dbname'].match?(/^[a-zA-Z0-9_-]+$/)

    data    = git_get
    user    = git_user_from_key(params['key'])
    comment = params.key?('comment') ? params['comment'] : ''

    # Make sure the non-prefix database name is used
    dbname = params['dbname'].gsub(/^#{user}_/, '')

    # Check if db does not already exist
    if data[user]['properties'].key?('mysql_dbs') && (data[user]['properties']['mysql_dbs'].key? dbname)
      halt erb :error, locals: { code: 409, message: 'MySQL database with the requested dbname already exists' }
    end

    request_submit(user, 'dbname' => dbname, 'comment' => comment)

    erb :result, locals: { message: 'MySQL database request received' }
  end

  # Generate a new password for MySQL database
  post '/v1/mysql/password/reset' do
    check_key(params)
    check_param(params, 'dbname')

    data = git_get
    user = git_user_from_key(params['key'])

    # Check if there are any database
    halt erb :error, locals: { code: 404, message: 'No MySQL databases configured' } unless data[user]['properties'].key? 'mysql_dbs'

    # Make sure the non-prefix database name is used
    dbname = params['dbname'].gsub(/^#{user}_/, '')

    # Check if the specified database exists
    halt erb :error, locals: { code: 404, message: 'No MySQL database by that name' } unless data[user]['properties']['mysql_dbs'].key? dbname

    # Set the new password
    dbpass = generate_password
    data[user]['properties']['mysql_dbs'][dbname] = { 'password' => dbpass }
    git_put data, "user: #{user}, dbname: #{dbname}"

    erb :result, locals: {
      message: 'Database configured',
      result: {
        'db user' => params['dbname'],
        'db name' => params['dbname'],
        'db pass' => dbpass
      }
    }
  end

  # Add a new database
  post '/v1/mysql/new' do
    check_key_admin(params)
    check_params(params, %w[username dbname])

    # Check db name for bad chars
    halt erb :error, locals: { code: 400, message: 'Illegal character in database name' } unless params['dbname'].match?(/^[a-zA-Z0-9_-]+$/)

    data = git_get
    user = params['username']

    # Check if db does not already exist
    halt erb :error, locals: { code: 404, message: 'User not found' } unless data.key? user
    if data[user]['properties'].key? 'mysql_dbs'
      if data[user]['properties']['mysql_dbs'].key? params['dbname']
        halt erb :error, locals: { code: 409, message: 'MySQL database with the requested dbname already exists' }
      end
    else
      data[user]['properties']['mysql_dbs'] = {}
    end

    # Configure new database
    dbpass = generate_password
    data[user]['properties']['mysql_dbs'][params['dbname']] = { 'password' => dbpass }
    git_put data, "user: #{user}, dbname: #{params['dbname']}"

    erb :result, locals: {
      message: 'Database configured',
      result: {
        'db user' => "#{user}_#{params['dbname']}",
        'db name' => "#{user}_#{params['dbname']}",
        'db pass' => dbpass,
        'info'    => 'You can use https://adminer.insomnia247.nl/ for if needed.'
      }
    }
  end
end
