# frozen_string_literal: true

# Test functions
class Manager2Api < Sinatra::Base
  # Function to help people test if their implemntation is working
  get '/v1/test' do
    erb :result, locals: { message: 'Test successfull' }
  end

  # Function to help people test if their implemntation is working
  post '/v1/test/key' do
    check_key(params)
    user = git_user_from_key(params['key'])
    erb :result, locals: { message: 'Test successfull', result: "API key belongs to #{user}." }
  end
end
