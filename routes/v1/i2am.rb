# frozen_string_literal: true

# Functions for i2am project
class Manager2Api < Sinatra::Base
  # List configured tunnels
  post '/v1/i2am/tunnel/list' do
    check_key(params)
    result = git_get_full['profile::sshtunnel::destination::tunnels']
    erb :result, locals: { message: 'Retreived configured tunnels', result: result }
  end

  # List configured TOR hidden services
  post '/v1/i2am/torservice/list' do
    check_key(params)
    result = git_get_full['i2am::torservices']
    erb :result, locals: { message: 'Retreived TOR services', result: result }
  end
end
