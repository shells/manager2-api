# frozen_string_literal: true

# rubocop:disable Metrics/ClassLength
# Functions for working with user account settings
class Manager2Api < Sinatra::Base
  # Check if API key can do admin-only commands
  post '/v1/user/admin' do
    check_key_admin(params)
    erb :result, locals: { message: 'API key has admin rights' }
  end

  # Check if user exists
  get '/v1/user/check/:username' do
    user = params['username'].gsub(/[^0-9a-zA-Z\-_]/, '')
    res  = user_check(user)
    erb :result, locals: { message: res ? 'User exists' : 'User does not exist', result: res }
  end

  # Create new user account
  post '/v1/user/new' do
    check_key_admin(params)
    check_param(params, 'username')

    user_create params['username']
    erb :result, locals: { message: 'User created' }
  end

  # Mark user account as removed
  post '/v1/user/remove' do
    check_key_admin(params)
    check_param(params, 'username')

    user_remove params['username']
    erb :result, locals: { message: 'User marked as removed' }
  end

  # Delete user account
  post '/v1/user/delete' do
    check_key_admin(params)
    check_param(params, 'username')

    user_delete params['username']
    erb :result, locals: { message: 'User deleted' }
  end

  # Check if user is archived
  post '/v1/user/archived' do
    check_key(params)
    user = git_user_from_key(params['key'])

    result  = user_check_archived user
    message = result ? 'User is archived' : 'User is not archived'
    erb :result, locals: { message: message, result: result }
  end

  # Check if list of users can be archived
  post '/v1/user/archivable' do
    check_key_admin(params)
    check_param(params, 'usernames')

    result = users_check_archivable params['usernames']
    erb :result, locals: { message: 'Archivable users', result: result }
  end

  # Mark list of users as archived
  post '/v1/user/archive' do
    check_key_admin(params)
    check_param(params, 'usernames')

    result = users_archive params['usernames']
    erb :result, locals: { message: 'Users archived', result: result }
  end

  # Mark shallow archived users as deep archived
  post '/v1/user/deeparchive' do
    check_key_admin(params)

    result = users_deep_archive
    erb :result, locals: { message: 'Users deep archived', result: result }
  end

  # Unmark user as archived
  post '/v1/user/unarchive' do
    check_key(params)
    user = git_user_from_key(params['key'])

    result  = user_unarchive user
    message = result ? 'User unarchived' : 'User was not archived'
    erb :result, locals: { message: message, result: result }
  end

  # Send a follow up email one week after account creation
  post '/v1/user/oneweekfollowup' do
    check_key_admin(params)
    check_param(params, 'username')
    user = params['username']

    result  = user_oneweekfollowup user
    message = 'Mail queued'
    erb :result, locals: { message: message, result: result }
  end

  # Get currently set shell from LDAP
  post '/v1/user/shell/get' do
    check_key(params)
    user = git_user_from_key(params['key'])
    erb :result, locals: { message: 'Retreived login shell', result: ldap_get_shell(user) }
  end

  # Set shell in LDAP
  post '/v1/user/shell/set' do
    check_key(params)
    check_param(params, 'shell')
    user = git_user_from_key(params['key'])

    halt erb :error, locals: { code: 400, message: 'Cannot set this as your login shell' } unless ldap_set_shell(user, params['shell'])

    erb :result, locals: { message: 'Login shell configured' }
  end

  # Reset user API key
  post '/v1/user/apikey/reset' do
    check_key_admin(params)
    check_param(params, 'username')

    data = git_get
    halt erb :error, locals: { code: 404, message: 'Can not find specified user' } unless data.key? params['username']

    newkey = generate_api_key
    data[params['username']]['properties']['api_key'] = newkey
    git_put data, "New API key for #{params['username']}."

    erb :result, locals: { message: 'New API key generated', result: newkey }
  end

  # Reset user password
  post '/v1/user/password/reset' do
    check_key_admin(params)
    check_param(params, 'username')

    ldap_password_reset params['username']

    erb :result, locals: { message: 'Password reset' }
  end

  # Check if ssh key only authentication is enabled
  post '/v1/user/auth/sshonly/check' do
    check_key(params)
    user    = git_user_from_key(params['key'])
    result  = ldap_check_user_in_group(user, 'ssh-no-password')
    message = result ? 'Ssh key only is enabled' : 'Ssh key only is not enabled'

    erb :result, locals: { message: message, result: result }
  end

  # Enable ssh key only authentication
  post '/v1/user/auth/sshonly/enable' do
    check_key(params)
    user = git_user_from_key(params['key'])

    halt erb :error, locals: { code: 400, message: 'Already enabled for this account' } if ldap_check_user_in_group(user, 'ssh-no-password')

    erb :result, locals: { message: 'Configured ssh key only login', result: ldap_add_user_to_group(user, 'ssh-no-password') }
  end

  # Disable ssh key only authentication
  post '/v1/user/auth/sshonly/disable' do
    check_key(params)
    user = git_user_from_key(params['key'])

    halt erb :error, locals: { code: 400, message: 'Already disabled for this account' } unless ldap_check_user_in_group(user, 'ssh-no-password')

    erb :result, locals: { message: 'Un-configured ssh key only login', result: ldap_remove_user_from_group(user, 'ssh-no-password') }
  end

  # List pending requests
  post '/v1/user/requests/list' do
    check_key(params)
    user = git_user_from_key(params['key'])

    erb :result, locals: { message: 'Retreived pending requests', result: requests_pending(user) }
  end

  # Retrieve configured contact email address
  post '/v1/user/email/get' do
    check_key(params)
    user = git_user_from_key(params['key'])

    erb :result, locals: {
      message: 'Retreived contact email address',
      result: mysql_get_contactmail(user)
    }
  end

  # Set a contact email address
  post '/v1/user/email/set' do
    check_key(params)
    check_param(params, 'email')

    unless params['email'].match?(/^.+@.+\.[a-zA-Z0-9]+$/)
      halt erb :error, locals: { code: 400, message: 'Provided email address does not look valid' }
    end

    user = git_user_from_key(params['key'])
    mysql_set_contactmail params['email'], user

    erb :result, locals: { message: 'Set new email contact address' }
  end

  # Do a lookup for the contact email address of another user
  post '/v1/user/email/lookup' do
    check_key_admin(params)
    check_param(params, 'username')

    halt erb :error, locals: { code: 400, message: 'User not found' } unless user_check(params['username'])

    erb :result, locals: {
      message: 'Retreived contact email address',
      result: mysql_get_contactmail(params['username'])
    }
  end

  # Send an invite
  post '/v1/user/invite/new' do
    check_key(params)
    check_param(params, 'email')
    user = git_user_from_key(params['key'])

    unless params['email'].match?(/^.+@.+\.[a-zA-Z0-9]+$/)
      halt erb :error, locals: { code: 400, message: 'Provided email address does not look valid' }
    end

    halt erb :error, locals: { code: 409, message: 'Provided email address was already used' } unless mysql_check_email params['email']

    # Check if user is allowed to generate an invite
    mysql_check_inviteable user

    # Generate the invite
    invite = generate_invite_code until mysql_check_invite invite

    mysql_insert_invite params['email'], invite, user

    # Send email to invitee
    notify_mail(
      params['email'],
      "#{user} has invited you to join Insomnia 24/7",
      (erb :'email/invite', locals: {
        username: user,
        invite: invite
      })
    )

    erb :result, locals: { message: 'Invite sent', result: invite }
  end
end
# rubocop:enable Metrics/ClassLength
