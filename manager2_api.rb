# frozen_string_literal: true

require 'date'
require 'git'
require 'json'
require 'logger'
require 'mysql2'
require 'net/http'
require 'net/ldap'
require 'pony'
require 'securerandom'
require 'sinatra/base'
require 'sinatra/custom_logger'
require 'yaml'

# Main class for api endpoints
class Manager2Api < Sinatra::Base
  helpers Sinatra::CustomLogger

  enable :lock
  set :config, YAML.load_file('config.yaml')

  configure :development, :production do
    logfile = File.open("#{root}/logs/#{environment}.log", 'a')
    logfile.sync = true
    logger = Logger.new(logfile)
    set :logger, logger
  end

  get '/' do
    'Nobody here but us chickens'
  end
end

require_relative 'helpers/ldap'
require_relative 'helpers/limits'
require_relative 'helpers/mysql'
require_relative 'helpers/notifications'
require_relative 'helpers/puppet'
require_relative 'helpers/requests'
require_relative 'helpers/rundeck'
require_relative 'helpers/users'
require_relative 'helpers/v1'
require_relative 'routes/v1/apache'
require_relative 'routes/v1/i2am'
require_relative 'routes/v1/limit'
require_relative 'routes/v1/mysql'
require_relative 'routes/v1/package'
require_relative 'routes/v1/requestadmin'
require_relative 'routes/v1/test'
require_relative 'routes/v1/user'
