API to allow limited access to puppet configuration and LDAP

## Quick function reference

### Function: `POST /v1/apache/vhost/list`:
List configured vhosts
 - Requires API key in `key` parameter to access

### Function: `POST /v1/apache/vhost/request`:
Request a new vhost
 - Requires API key in `key` parameter to access
 - Requires additional parameter to be set: `domain`
 - Allows optional parameter to be set: `comment`
 - Allows optional parameter to be set: `documentroot`
 - Allows optional parameter to be set: `strongtls`

### Function: `POST /v1/apache/vhost/remove`:
Remove a vhost
 - Requires API key in `key` parameter to access
 - Requires additional parameter to be set: `domain`

### Function: `POST /v1/apache/vhost/disable`:
Disable a vhost
 - Requires API key in `key` parameter to access
 - Requires additional parameter to be set: `domain`

### Function: `POST /v1/apache/vhost/enable`:
Enable a vhost
 - Requires API key in `key` parameter to access
 - Requires additional parameter to be set: `domain`

### Function: `POST /v1/apache/vhost/new`:
Create a new vhost
 - Requires API key in `key` parameter to access
 - Requires administrative privileges
 - Requires additional parameters to be set: `domain`, `username`
 - Allows optional parameter to be set: `strongtls`
 - Allows optional parameter to be set: `documentroot`

### Function: `POST /v1/i2am/tunnel/list`:
List configured tunnels
 - Requires API key in `key` parameter to access

### Function: `POST /v1/i2am/torservice/list`:
List configured TOR hidden services
 - Requires API key in `key` parameter to access

### Function: `POST /v1/limit/list`:
List configured limites
 - Requires API key in `key` parameter to access

### Function: `POST /v1/limit/disk/request`:
Request new disk quota
 - Requires API key in `key` parameter to access
 - Requires additional parameter to be set: `limit`
 - Allows optional parameter to be set: `comment`

### Function: `POST /v1/limit/disk/set`:
Set a users disk quota
 - Requires API key in `key` parameter to access
 - Requires administrative privileges
 - Requires additional parameters to be set: `username`, `limit`

### Function: `POST /v1/limit/disk/get`:
Get a users current disk quota
 - Requires API key in `key` parameter to access
 - Requires administrative privileges
 - Requires additional parameter to be set: `username`

### Function: `POST /v1/limit/process/request`:
Request higher process count limit
 - Requires API key in `key` parameter to access
 - Allows optional parameter to be set: `comment`

### Function: `POST /v1/limit/process/set`:
Set higher process count limit
 - Requires API key in `key` parameter to access
 - Requires administrative privileges
 - Requires additional parameter to be set: `username`

### Function: `POST /v1/limit/cpu/request`:
Request new cpu shares cap
 - Requires API key in `key` parameter to access
 - Requires additional parameter to be set: `limit`
 - Allows optional parameter to be set: `comment`

### Function: `POST /v1/limit/cpu/set`:
Set a users cpu shares cap
 - Requires API key in `key` parameter to access
 - Requires administrative privileges
 - Requires additional parameters to be set: `username`, `limit`

### Function: `POST /v1/limit/cpu/get`:
Get a users cpu shares cap
 - Requires API key in `key` parameter to access
 - Requires administrative privileges
 - Requires additional parameter to be set: `username`

### Function: `POST /v1/limit/memory/request`:
Request new memory usage cap
 - Requires API key in `key` parameter to access
 - Requires additional parameter to be set: `limit`
 - Allows optional parameter to be set: `comment`

### Function: `POST /v1/limit/memory/set`:
Set a users memory usage cap
 - Requires API key in `key` parameter to access
 - Requires administrative privileges
 - Requires additional parameters to be set: `username`, `limit`

### Function: `POST /v1/limit/memory/get`:
Get a users memory usage cap
 - Requires API key in `key` parameter to access
 - Requires administrative privileges
 - Requires additional parameter to be set: `username`

### Function: `POST /v1/mysql/list`:
List configured databases
 - Requires API key in `key` parameter to access

### Function: `POST /v1/mysql/request`:
Request a database
 - Requires API key in `key` parameter to access
 - Requires additional parameter to be set: `dbname`
 - Allows optional parameter to be set: `comment`

### Function: `POST /v1/mysql/password/reset`:
Generate a new password for MySQL database
 - Requires API key in `key` parameter to access
 - Requires additional parameter to be set: `dbname`

### Function: `POST /v1/mysql/new`:
Add a new database
 - Requires API key in `key` parameter to access
 - Requires administrative privileges
 - Requires additional parameters to be set: `username`, `dbname`

### Function: `POST /v1/package/request`:
Request package install
 - Requires API key in `key` parameter to access
 - Requires additional parameter to be set: `package`
 - Allows optional parameter to be set: `comment`
 - Allows optional parameter to be set: `backports`

### Function: `POST /v1/package/new`:
Install a package
 - Requires API key in `key` parameter to access
 - Requires administrative privileges
 - Requires additional parameter to be set: `package`
 - Allows optional parameter to be set: `backports`

### Function: `POST /v1/request/delete`:
Delete a pending request
 - Requires API key in `key` parameter to access
 - Requires administrative privileges
 - Requires additional parameter to be set: `id`

### Function: `POST /v1/request/list`:
List all pending requests
 - Requires API key in `key` parameter to access
 - Requires administrative privileges

### Function: `POST /v1/notify/irc`:
Send notification via IRC
 - Requires API key in `key` parameter to access
 - Requires administrative privileges
 - Requires additional parameters to be set: `username`, `message`

### Function: `POST /v1/notify/email`:
Send notification via email
 - Requires API key in `key` parameter to access
 - Requires administrative privileges
 - Requires additional parameters to be set: `username`, `subject`, `body`

### Function: `POST /v1/notify/email/approved`:
Send email with request approved mail template
 - Requires API key in `key` parameter to access
 - Requires administrative privileges
 - Requires additional parameters to be set: `username`, `details`

### Function: `POST /v1/notify/email/rejected`:
Send email with request rejected mail template
 - Requires API key in `key` parameter to access
 - Requires administrative privileges
 - Requires additional parameters to be set: `username`, `details`, `comments`

### Function: `GET /v1/test`:
Function to help people test if their implemntation is working

### Function: `POST /v1/test/key`:
Function to help people test if their implemntation is working
 - Requires API key in `key` parameter to access

### Function: `POST /v1/user/admin`:
Check if API key can do admin-only commands
 - Requires API key in `key` parameter to access
 - Requires administrative privileges

### Function: `GET /v1/user/check/:username`:
Check if user exists

### Function: `POST /v1/user/new`:
Create new user account
 - Requires API key in `key` parameter to access
 - Requires administrative privileges
 - Requires additional parameter to be set: `username`

### Function: `POST /v1/user/remove`:
Mark user account as removed
 - Requires API key in `key` parameter to access
 - Requires administrative privileges
 - Requires additional parameter to be set: `username`

### Function: `POST /v1/user/delete`:
Delete user account
 - Requires API key in `key` parameter to access
 - Requires administrative privileges
 - Requires additional parameter to be set: `username`

### Function: `POST /v1/user/archived`:
Check if user is archived
 - Requires API key in `key` parameter to access

### Function: `POST /v1/user/archivable`:
Check if list of users can be archived
 - Requires API key in `key` parameter to access
 - Requires administrative privileges
 - Requires additional parameter to be set: `usernames`

### Function: `POST /v1/user/archive`:
Mark list of users as archived
 - Requires API key in `key` parameter to access
 - Requires administrative privileges
 - Requires additional parameter to be set: `usernames`

### Function: `POST /v1/user/deeparchive`:
Mark shallow archived users as deep archived
 - Requires API key in `key` parameter to access
 - Requires administrative privileges

### Function: `POST /v1/user/unarchive`:
Unmark user as archived
 - Requires API key in `key` parameter to access

### Function: `POST /v1/user/oneweekfollowup`:
Send a follow up email one week after account creation
 - Requires API key in `key` parameter to access
 - Requires administrative privileges
 - Requires additional parameter to be set: `username`

### Function: `POST /v1/user/shell/get`:
Get currently set shell from LDAP
 - Requires API key in `key` parameter to access

### Function: `POST /v1/user/shell/set`:
Set shell in LDAP
 - Requires API key in `key` parameter to access
 - Requires additional parameter to be set: `shell`

### Function: `POST /v1/user/apikey/reset`:
Reset user API key
 - Requires API key in `key` parameter to access
 - Requires administrative privileges
 - Requires additional parameter to be set: `username`

### Function: `POST /v1/user/password/reset`:
Reset user password
 - Requires API key in `key` parameter to access
 - Requires administrative privileges
 - Requires additional parameter to be set: `username`

### Function: `POST /v1/user/auth/sshonly/check`:
Check if ssh key only authentication is enabled
 - Requires API key in `key` parameter to access

### Function: `POST /v1/user/auth/sshonly/enable`:
Enable ssh key only authentication
 - Requires API key in `key` parameter to access

### Function: `POST /v1/user/auth/sshonly/disable`:
Disable ssh key only authentication
 - Requires API key in `key` parameter to access

### Function: `POST /v1/user/requests/list`:
List pending requests
 - Requires API key in `key` parameter to access

### Function: `POST /v1/user/email/get`:
Retrieve configured contact email address
 - Requires API key in `key` parameter to access

### Function: `POST /v1/user/email/set`:
Set a contact email address
 - Requires API key in `key` parameter to access
 - Requires additional parameter to be set: `email`

### Function: `POST /v1/user/email/lookup`:
Do a lookup for the contact email address of another user
 - Requires API key in `key` parameter to access
 - Requires administrative privileges
 - Requires additional parameter to be set: `username`

### Function: `POST /v1/user/invite/new`:
Send an invite
 - Requires API key in `key` parameter to access
 - Requires additional parameter to be set: `email`
