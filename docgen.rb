# frozen_string_literal: true

files    = Dir.glob('routes/**/*\.rb')
lastline = ''

files.each do |file|
  content = File.read(file)

  content.each_line do |line|
    case line
    when /(get|post) '(.+)' do/i
      puts "\n### Function: `#{Regexp.last_match(1).upcase} #{Regexp.last_match(2)}`:"

      puts Regexp.last_match(1) if !lastline.match?(/rubocop/) && (lastline =~ /^\s*#\s+(.+)$/)
    when /check_key\(params\)/
      puts ' - Requires API key in `key` parameter to access'
    when /check_key_admin\(params\)/
      puts " - Requires API key in `key` parameter to access\n - Requires administrative privileges"
    when /check_param\(params,\s*'(.+)'\)/
      puts " - Requires additional parameter to be set: `#{Regexp.last_match(1)}`"
    when /check_params\(params,\s*%w\[(.+)\]\)/
      puts " - Requires additional parameters to be set: `#{Regexp.last_match(1).split.join('`, `')}`"
    when /params\.key\?[\s(]*'(.+?)'/
      puts " - Allows optional parameter to be set: `#{Regexp.last_match(1)}`"
    end
    lastline = line
  end
end
