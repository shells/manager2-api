# frozen_string_literal: true

# Helper functions for manipulating limits
class Manager2Api < Sinatra::Base
  # rubocop:disable Metrics/BlockLength
  helpers do
    # Disk quotas
    def limit_disk_get(username)
      data = git_get
      return 'No disk quota set' unless data[username]['properties'].key? 'quota'

      data[username]['properties']['quota']
    end

    def limit_disk_set(username, limit)
      data = git_get
      data[username]['properties']['quota'] =
        { 'soft' => "#{limit.to_i}M", 'hard' => "#{limit.to_i + 250}M" }
      git_put data, "Set disk quota; user: #{username}, limit: #{limit}M"
    end

    # CPU limits
    def limit_cpu_get(username)
      data = git_get
      return 'No CPU cap set' unless data[username]['properties'].key? 'cgroups'

      return 'No CPU cap set' unless data[username]['properties']['cgroups'].key? 'cpucap'

      data[username]['properties']['cgroups']['cpucap']
    end

    def limit_cpu_set(username, limit)
      data = git_get
      data[username]['properties']['cgroups'] = {} unless data[username]['properties'].key? 'cgroups'

      data[username]['properties']['cgroups']['cpucap'] = limit
      git_put data, "Set CPU cap; user: #{username}, cap: #{limit}%"
    end

    # Memory usage limits
    def limit_memory_get(username)
      data = git_get
      return 'No memory limit set, default is 4GB' unless data[username]['properties'].key? 'cgroups'

      return 'No memory limit set, default is 4GB' unless data[username]['properties']['cgroups'].key? 'memory'

      data[username]['properties']['cgroups']['memory']
    end

    def limit_memory_set(username, limit)
      data = git_get
      data[username]['properties']['cgroups'] = {} unless data[username]['properties'].key? 'cgroups'

      data[username]['properties']['cgroups']['memory'] = "#{limit}g"
      git_put data, "Set memory limit; user: #{username}, limit: #{limit}g"
    end
  end
  # rubocop:enable Metrics/BlockLength
end
