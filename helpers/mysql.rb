# frozen_string_literal: true

# MySQL connector and database helper functions
class Manager2Api < Sinatra::Base
  # rubocop:disable Metrics/BlockLength
  helpers do
    before do
      @sqldb = mysql_connect
    end
    after do
      @sqldb&.close
    end

    # rubocop:disable Metrics/AbcSize
    # rubocop:disable Metrics/MethodLength
    def mysql_connect
      Mysql2::Client.new(
        host: settings.config['mysql']['server'],
        port: settings.config['mysql']['port'],
        database: settings.config['mysql']['database'],
        username: settings.config['mysql']['username'],
        password: settings.config['mysql']['password'],
        connect_timeout: settings.config['mysql']['timeout']
      )
    rescue Mysql2::Error => e
      log_error "Cannot connect to MySQL server: #{e}"
      halt erb :error, locals: { code: 500, message: 'Internal error: Contact administrator' }
    end
    # rubocop:enable Metrics/MethodLength
    # rubocop:enable Metrics/AbcSize

    # Query user record from database
    def mysql_get_record(username)
      statement = @sqldb.prepare 'SELECT * FROM invites WHERE `username` = ? LIMIT 1'
      result    = statement.execute username
      result.first
    end

    # Insert new invite into the database
    def mysql_insert_invite(email, invite, username)
      # Insert invitee
      insert = @sqldb.prepare 'INSERT INTO invites (`email`, `key`, `parent`) VALUES (?, ?, ?)'
      insert.execute email, invite, username
      log_info "Inserted invite from #{username} into database"

      # Update inviter
      mysql_update_last_invite username
    end

    # Set date last invite was sent to today
    def mysql_update_last_invite(username)
      update = @sqldb.prepare 'UPDATE invites SET `last` = ? WHERE `username` = ?'
      update.execute Date.today.to_s, username

      log_info "Updated last invite date for: #{username}"
    end

    # Functions to help check invite parameters (check for duplicate keys or emails)
    def mysql_check_invite(invite)
      return false if invite.nil?

      statement = @sqldb.prepare 'SELECT COUNT(*) FROM invites WHERE `key` = ?'
      result    = statement.execute invite
      result.first['COUNT(*)'] < 1
    end

    def mysql_check_email(email)
      statement = @sqldb.prepare 'SELECT COUNT(*) FROM invites WHERE `email` = ?'
      result    = statement.execute email
      result.first['COUNT(*)'] < 1
    end

    # Check if user is allowed to generate invites
    def mysql_check_inviteable(username)
      userdata = mysql_get_record username

      # User may generate infinite invites
      return true if userdata['privs'].to_i == 1

      # Check user has not generated an invite too recently
      daysago  = (Date.today - userdata['last']).to_i
      waittime = 30 * (userdata['strikes'].to_i + 1)
      return true if daysago > waittime

      # Fail if no conditions permitted invite generation
      halt erb :error, locals: {
        code: 401,
        message: "Must wait #{waittime - daysago} days to generate another invite"
      }
    end

    # Check if a user exists in the invite db
    def mysql_check_user(username)
      statement = @sqldb.prepare 'SELECT COUNT(*) FROM invites WHERE `username` = ?'
      result    = statement.execute username
      result.first['COUNT(*)'].positive?
    end

    # Set a new contact email
    def mysql_set_contactmail(email, username)
      update = @sqldb.prepare 'UPDATE invites SET `email` = ? WHERE `username` = ?'
      update.execute email, username
      log_info "Updated contact email for user: #{username}"
    end

    # Get contact mail for user
    def mysql_get_contactmail(username)
      record = mysql_get_record(username)
      return record['email'] unless record['email'].empty?

      "#{username}@insomnia247.nl"
    end
  end
  # rubocop:enable Metrics/BlockLength
end
