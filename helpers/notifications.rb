# frozen_string_literal: true

# Internal helper functions
class Manager2Api < Sinatra::Base
  # rubocop:disable Metrics/BlockLength
  helpers do
    def notify_mail_user(username, subject, body)
      notify_mail mysql_get_contactmail(username), subject, body
    end

    def notify_mail_admins(subject, body)
      settings.config['notify']['mail']['admins'].each do |to|
        notify_mail to, subject, body
      end
    end

    # rubocop:disable Metrics/AbcSize
    # rubocop:disable Metrics/MethodLength
    def notify_mail(email, subject, body)
      Pony.mail(
        to: email,
        from: settings.config['notify']['mail']['from'],
        reply_to: settings.config['notify']['mail']['reply_to'],
        subject: subject,
        body: body,
        via: :smtp,
        via_options: {
          address: settings.config['notify']['mail']['server'],
          enable_starttls_auto: true
        }
      )
      log_info "Sent email to #{email}"
    rescue Net::SMTPFatalError => e
      halt erb :error, locals: {
        code: 500,
        message: 'Internal error: Contact administrator'
      }

      log_error "Failed to send email to #{email}"
      log_error e.to_s
    end
    # rubocop:enable Metrics/MethodLength
    # rubocop:enable Metrics/AbcSize

    # rubocop:disable Metrics/AbcSize
    # rubocop:disable Metrics/MethodLength
    def notify_irc(message, channels: [settings.config['notify']['irc']['channel']], prefix: true)
      message = "#{api_route_name}: #{message}" if prefix
      uri     = URI settings.config['notify']['irc']['endpoint']

      channels.each do |channel|
        res = Net::HTTP.post_form(
          uri,
          'key'     => settings.config['notify']['irc']['key'],
          'dest'    => channel,
          'message' => message
        )

        if res.is_a? Net::HTTPSuccess
          log_info "Sent notification to IRC: #{channel}"
        else
          log_error "Failed to send message to IRC channel: #{channel}"
        end
      end
    end
    # rubocop:enable Metrics/MethodLength
    # rubocop:enable Metrics/AbcSize

    def notify_irc_admins(message)
      notify_irc message, channels: settings.config['notify']['irc']['admins']
    end
  end
  # rubocop:enable Metrics/BlockLength
end
