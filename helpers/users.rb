# frozen_string_literal: true

# Helper functions for user management
# rubocop:disable Metrics/ClassLength
class Manager2Api < Sinatra::Base
  # rubocop:disable Metrics/BlockLength

  require 'string/crypt'

  helpers do
    # Generate a new API key
    def generate_api_key
      SecureRandom.hex(32)
    end

    # Generate new invite code
    # For now we just use the same code as generating
    # an api key.
    def generate_invite_code
      generate_api_key
    end

    # Generate a new password
    def generate_password(length = 24)
      charset = 'abcdefghijkmnpqrstuvwxyzBCDEFGHJKLMNPQRSTUVWXYZ236789'.chars
      newpass = []
      length.times do
        newpass.push charset[SecureRandom.random_number(charset.size)]
      end
      newpass.join
    end

    # Generate a password hash
    def generate_password_hash(password, nologin)
      return '{crypt}!' if nologin

      pwhash = password.crypt "$6$#{SecureRandom.alphanumeric 16}$"
      "{crypt}#{pwhash}"
    end

    # Create a list of users
    # rubocop:disable Metrics/MethodLength
    # rubocop:disable Metrics/AbcSize
    def user_create(usernames)
      data = git_get
      done = []

      usernames.split(',').each do |username|
        # Create new group
        gid = ldap_create_group username

        # Create new user
        password = ldap_create_user username, gid, "/home/#{username}"

        # Create new apache sub-user
        ldap_create_user "#{username}-www", gid, "/home/#{username}", true

        # Add new user to default group for all users
        ldap_add_user_to_group username, 'users'

        # Configure puppet
        userconfig = {
          'properties' => {
            'api_key'       => generate_api_key,
            'apache_vhosts' => { "#{username}.insomnia247.nl" => {} },
            'uidmap_offset' => git_get_free_uidmap_offset
          }
        }

        data[username] = userconfig

        # Check if user has a record in the invite db
        if mysql_check_user username
          # Send welcome and password emails
          notify_mail_user(
            username,
            'Welcome to Insomnia 24/7!',
            (erb :'email/newuser_welcome', locals: { username: username })
          )

          notify_mail_user(
            username,
            'Your shell password',
            (erb :'email/newuser_password', locals: { password: password })
          )

          # Update invite database
          mysql_update_last_invite username

          # Send notification to IRC
          notify_irc "Welcome to our newest user: #{username}!", prefix: false

          # Schedule a follow-up email to be sent in 1 week
          schedule_followup username
        else
          # Notify IRC staff
          notify_irc_admins "User created: #{username} but no invite db record exists."
          log_error "No invite db record for user: #{username}"
        end
        done.push username
      end

      git_put data, 'Added user(s) to puppet configuration'
      log_info "Created user(s): #{done.join ','}"
    end
    # rubocop:enable Metrics/AbcSize
    # rubocop:enable Metrics/MethodLength

    # Mark a list of users as removed
    # rubocop:disable Metrics/AbcSize
    # rubocop:disable Metrics/MethodLength
    def user_remove(usernames)
      # Configure puppet
      data = git_get
      done = []

      usernames.split(',').each do |username|
        data[username]['properties']['removed'] = true

        # Set removed on apache vhosts and proxies
        if data[username]['properties'].key? 'apache_vhosts'
          data[username]['properties']['apache_vhosts'].each_key do |vhost|
            data[username]['properties']['apache_vhosts'][vhost]['removed'] = true
          end
        end

        if data[username]['properties'].key? 'apache_proxies'
          data[username]['properties']['apache_proxies'].each_key do |vhost|
            data[username]['properties']['apache_proxies'][vhost]['removed'] = true
          end
        end

        # Reset password
        ldap_password_reset username, false
        done.push username
      end

      git_put data, 'Marked user(s) as removed'
      log_info "Marked user(s) as removed: #{done.join ','}"
    end
    # rubocop:enable Metrics/AbcSize
    # rubocop:enable Metrics/MethodLength

    # Completely delete a list of configured users
    # rubocop:disable Metrics/MethodLength
    def user_delete(usernames)
      data = git_get
      done = []

      usernames.split(',').each do |username|
        next unless data.key? username

        # Check if user is in special groups
        %w[users power-users raised-proc raised-mem].each do |groupname|
          ldap_remove_user_from_group username, groupname if ldap_check_user_in_group username, groupname
        end

        # Remove user
        ldap_delete_user username

        # Remove apache sub-user
        ldap_delete_user "#{username}-www"

        # Remove group
        ldap_delete_group username

        # Remove puppet config
        data.delete username

        done.push username
      end

      git_put data, 'Deleted user(s) from puppet configuration'
      log_info "Removed user(s): #{done.join ','}"
    end
  end
  # rubocop:enable Metrics/MethodLength

  # Check if user exists
  def user_check(username)
    data = git_get
    data.keys.map(&:downcase).include? username.downcase
  end

  # Check if user is archived
  def user_check_archived(username)
    data = git_get
    data[username]['properties'].key? 'archived'
  end

  # Check if user is archivable
  def users_check_archivable(usernames)
    data   = git_get
    result = []
    usernames.split(',').each do |username|
      next unless data.key? username

      archivable = true
      archivable = false if data[username]['properties'].key? 'archived'
      archivable = false if data[username]['properties'].key? 'archivable'
      result.push username if archivable
    end

    result.join ','
  end

  # Archive a list of users
  # rubocop:disable Metrics/AbcSize
  # rubocop:disable Metrics/MethodLength
  def users_archive(usernames)
    data = git_get
    done = []

    usernames.split(',').each do |username|
      next unless data.key? username

      data[username]['properties']['archived'] = true
      data[username]['properties']['motd']     = erb :'notification/archived'

      # Set archived on apache vhosts and proxies
      if data[username]['properties'].key? 'apache_vhosts'
        data[username]['properties']['apache_vhosts'].each_key do |vhost|
          data[username]['properties']['apache_vhosts'][vhost]['archived'] = true
        end
      end

      if data[username]['properties'].key? 'apache_proxies'
        data[username]['properties']['apache_proxies'].each_key do |vhost|
          data[username]['properties']['apache_proxies'][vhost]['archived'] = true
        end
      end

      done.push username
    end

    git_put data, "Archived users: #{done}"
    log_info "Archived users: #{done}"

    done.join(',')
  end

  def users_deep_archive
    data = git_get
    done = []

    data.each do |username, config|
      if config['properties'].key?('archived') && config['properties']['archived'] == true
        data[username]['properties']['archived'] = 'deep'
        done.push username
      end
    end

    git_put data, "Deep archived users: #{done}"
    log_info "Deep archived users: #{done}"

    done.join(',')
  end

  # Unarchive a single user
  def user_unarchive(username)
    data = git_get
    return false unless data[username]['properties'].key? 'archived'

    data[username]['properties'].delete 'archived'
    data[username]['properties'].delete 'motd'

    # Unset archived on apache vhosts and proxies
    if data[username]['properties'].key? 'apache_vhosts'
      data[username]['properties']['apache_vhosts'].each_key do |vhost|
        data[username]['properties']['apache_vhosts'][vhost].delete 'archived'
      end
    end

    if data[username]['properties'].key? 'apache_proxies'
      data[username]['properties']['apache_proxies'].each_key do |vhost|
        data[username]['properties']['apache_proxies'][vhost].delete 'archived'
      end
    end

    trigger_unarchive username
    git_put data, "Unarchived user: #{username}", false
  end

  # Send a follow-up email one week after account creation
  def user_oneweekfollowup(username)
    notify_mail_user(
      username,
      'It has been one week. How are things going?',
      (erb :'email/newuser_followup', locals: { username: username })
    )
  end
  # rubocop:enable Metrics/AbcSize
  # rubocop:enable Metrics/MethodLength
  # rubocop:enable Metrics/BlockLength
end
# rubocop:enable Metrics/ClassLength
