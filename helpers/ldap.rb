# frozen_string_literal: true

# rubocop:disable Metrics/ClassLength
# Internal helper functions
class Manager2Api < Sinatra::Base
  before do
    @ldap = ldap_connect
  end

  # rubocop:disable Metrics/BlockLength
  helpers do
    # rubocop:disable Metrics/AbcSize
    # rubocop:disable Metrics/MethodLength
    # Connect to LDAP server and return LDAP connection object
    def ldap_connect
      connection = Net::LDAP.new(
        host: settings.config['ldap']['server'],
        port: 389,
        auth: {
          method: :simple,
          username: "#{settings.config['ldap']['user']},#{settings.config['ldap']['base']}",
          password: settings.config['ldap']['pass']
        }
      )

      begin
        connection.bind
      rescue Net::LDAP::Error => e
        log_error "Cannot bind to LDAP server: #{e}"
        ldap_error false
      end

      connection
    end
    # rubocop:enable Metrics/AbcSize
    # rubocop:enable Metrics/MethodLength

    # Throw generic ldap error response
    def ldap_error(objready = true)
      log_error @ldap.get_operation_result if objready

      halt erb :error, locals: {
        code: 500,
        message: 'Internal error: Contact administrator'
      }
    end

    # Query ldap for all ids of a certain type
    def ldap_get_all_ids(ouname, filter)
      @ldap.search(
        base: "#{ouname},#{settings.config['ldap']['base']}",
        filter: "#{filter}=*",
        attributes: [filter]
      ).map { |e| e[filter].first.to_i }
    end

    # Get first free id from a list
    def ldap_get_next_free_id(idlist, newid)
      newid += 1 while idlist.include? newid
      newid
    end

    # Get next free UID
    def ldap_get_next_uid(startuid = 1000)
      uids = ldap_get_all_ids settings.config['ldap']['oup'], 'uidnumber'
      ldap_get_next_free_id uids, startuid
    end

    # Get next free GID
    def ldap_get_next_gid(startgid = 1000)
      gids = ldap_get_all_ids settings.config['ldap']['oug'], 'gidnumber'
      ldap_get_next_free_id gids, startgid
    end

    # rubocop:disable Metrics/MethodLength
    # Create a group
    def ldap_create_group(groupname)
      groupdn  = "cn=#{groupname},#{settings.config['ldap']['oug']},#{settings.config['ldap']['base']}"
      newgid   = ldap_get_next_gid
      grpattrs = {
        cn: groupname,
        objectclass: %w[posixGroup top],
        userpassword: '{crypt}x',
        gidnumber: newgid.to_s
      }

      # Add to LDAP server
      unless @ldap.add(dn: groupdn, attributes: grpattrs)
        log_error "Failed to add LDAP object: #{groupdn}"
        ldap_error
      end

      log_info "Added LDAP object: #{groupdn}"
      newgid
    end
    # rubocop:enable Metrics/MethodLength

    # Remove a group
    def ldap_delete_group(groupname)
      groupdn = "cn=#{groupname},#{settings.config['ldap']['oug']},#{settings.config['ldap']['base']}"

      # Remove from LDAP server
      unless @ldap.delete(dn: groupdn)
        log_error "Failed to delete LDAP object: #{groupdn}"
        ldap_error
      end

      log_info "Deleted LDAP object: #{groupdn}"
    end

    # Check if a user is part of a group
    def ldap_check_user_in_group(username, groupname)
      @ldap.search(
        base: "#{settings.config['ldap']['oug']},#{settings.config['ldap']['base']}",
        filter: "cn=#{groupname}",
        attributes: ['memberuid'],
        return_result: true
      ).first['memberuid'].include? username
    end

    # rubocop:disable Metrics/MethodLength
    # Remove user from extra group
    def ldap_remove_user_from_group(username, groupname)
      groupdn = "cn=#{groupname},#{settings.config['ldap']['oug']},#{settings.config['ldap']['base']}"
      unless @ldap.modify(
        dn: groupdn,
        operations: [
          [:delete, :memberuid, username]
        ]
      )
        log_error "Failed to modify LDAP object: #{groupdn}"
        ldap_error
      end

      log_info "Modified LDAP object: #{groupdn}"
    end
    # rubocop:enable Metrics/MethodLength

    # rubocop:disable Metrics/MethodLength
    # Add user to extra group
    def ldap_add_user_to_group(username, groupname)
      groupdn = "cn=#{groupname},#{settings.config['ldap']['oug']},#{settings.config['ldap']['base']}"
      unless @ldap.modify(
        dn: groupdn,
        operations: [
          [:add, :memberuid, username]
        ]
      )
        log_error "Failed to modify LDAP object: #{groupdn}"
        ldap_error
      end

      log_info "Modified LDAP object: #{groupdn}"
    end
    # rubocop:enable Metrics/MethodLength

    # rubocop:disable Metrics/AbcSize
    # rubocop:disable Metrics/MethodLength
    # Create a user
    def ldap_create_user(username, groupid, homedir, nologin = false)
      password = generate_password
      pwhash   = generate_password_hash(password, nologin)
      newuid   = ldap_get_next_uid
      userdn   = "uid=#{username},#{settings.config['ldap']['oup']},#{settings.config['ldap']['base']}"
      lastch   = nologin ? '1' : '0'

      userattrs = {
        cn: username,
        gidnumber: groupid.to_s,
        homedirectory: homedir,
        loginshell: '/bin/bash',
        objectclass: %w[account posixAccount top shadowAccount],
        shadowlastchange: lastch,
        shadowmax: '99999',
        shadowwarning: '7',
        uid: username,
        uidnumber: newuid.to_s,
        userpassword: pwhash
      }

      # Add object to LDAP server
      unless @ldap.add(dn: userdn, attributes: userattrs)
        log_error "Failed to add LDAP object: #{userdn}"
        ldap_error
      end

      log_info "Added LDAP object: #{userdn}"
      password unless nologin
    end
    # rubocop:enable Metrics/AbcSize
    # rubocop:enable Metrics/MethodLength

    # Remove a user
    def ldap_delete_user(username)
      userdn = "uid=#{username},#{settings.config['ldap']['oup']},#{settings.config['ldap']['base']}"

      # Remove from LDAP server
      unless @ldap.delete(dn: userdn)
        log_error "Failed to delete LDAP object: #{userdn}"
        ldap_error
      end

      log_info "Deleted LDAP object: #{userdn}"
    end

    # rubocop:disable Metrics/MethodLength
    # Reset a password
    def ldap_password_reset(username, notify = true)
      password = generate_password
      pwhash   = generate_password_hash(password, false)
      userdn   = "uid=#{username},#{settings.config['ldap']['oup']},#{settings.config['ldap']['base']}"

      unless @ldap.modify(
        dn: userdn,
        operations: [
          [:replace, :userPassword, pwhash],
          [:replace, :shadowLastChange, '0']
        ]
      )
        log_error "Failed to modify LDAP object: #{userdn}"
        ldap_error
      end

      # Notify user of new password
      if notify
        notify_mail_user(
          username,
          'Password reset',
          (erb :'email/newuser_password', locals: { password: password })
        )
      end

      log_info "Modified LDAP object: #{userdn}"
      password
    end
    # rubocop:enable Metrics/MethodLength

    # Functions to configure login shell in LDAP server
    def ldap_get_shell(username)
      @ldap.search(
        base: "#{settings.config['ldap']['oup']},#{settings.config['ldap']['base']}",
        filter: "uid=#{username}",
        attributes: ['loginShell'],
        return_result: true
      ).first['loginShell'].first
    end

    # rubocop:disable Metrics/AbcSize
    # rubocop:disable Metrics/MethodLength
    def ldap_set_shell(username, shell)
      return false unless settings.config['ldap']['shells'].include? shell

      userdn = "uid=#{username},#{settings.config['ldap']['oup']},#{settings.config['ldap']['base']}"
      unless @ldap.modify(
        dn: userdn,
        operations: [
          [:replace, :loginShell, shell]
        ]
      )
        log_error "Failed to modify LDAP object: #{userdn}"
        ldap_error
      end

      log_info "Modified LDAP object: #{userdn}"
      log_info "Set shell to: #{shell}"
      true
    end
    # rubocop:enable Metrics/AbcSize
    # rubocop:enable Metrics/MethodLength
  end
  # rubocop:enable Metrics/BlockLength
end
# rubocop:enable Metrics/ClassLength
