# frozen_string_literal: true

# Internal helper functions
class Manager2Api < Sinatra::Base
  # rubocop:disable Metrics/BlockLength
  helpers do
    # Check if API key was submitted and is valid
    def check_key(params)
      # See if key was even submitted
      halt erb :error, locals: { code: 401, message: 'Must submit API key' } unless params.key? 'key'

      # See if key is valid
      halt erb :error, locals: { code: 401, message: 'Invalid API key' } unless git_user_from_key(params['key'])
    end

    # Check if key has admin rights
    def check_key_admin(params)
      # Start with normal checks
      check_key params

      # See if key belongs to an admin
      # rubocop:disable Style/GuardClause
      unless git_key_is_admin(params['key'])
        halt erb :error, locals: {
          code: 401,
          message: 'API key does not have admin rights'
        }
      end
      # rubocop:enable Style/GuardClause
    end

    # See if required parameters are provided
    def check_param(params, key)
      halt erb :error, locals: { code: 400, message: "Missing parameter: #{key}" } unless params.key? key
    end

    def check_params(params, keys)
      keys.each { |k| check_param(params, k) }
    end

    def api_route_name
      path = request.path_info.split('/').drop(1)
      path.map(&:capitalize!)
      path.join '::'
    end

    # Send messages to application log
    def log_info(message)
      logger.info "#{api_route_name}: #{message}"
    end

    def log_error(message)
      logger.error "#{api_route_name}: #{message}"
    end
  end
  # rubocop:enable Metrics/BlockLength
end
