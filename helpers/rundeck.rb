# frozen_string_literal: true

# Internal helper functions
class Manager2Api < Sinatra::Base
  require 'json'

  # rubocop:disable Metrics/BlockLength
  helpers do
    def trigger_unarchive(username)
      retries ||= 0

      uri = URI(settings.config['rundeck']['unarchive'])
      res = Net::HTTP.post(uri, { 'username' => username }.to_json, 'Content-Type' => 'application/json')
      log_info "Triggering unarchive: #{res}"
    rescue Net::ReadTimeout => e
      log_error "Timed out while trying to connect #{e} (Retries: #{retries})"
      retry if (retries += 1) < 3
    end

    # Generic function to call rundeck API and schedule a job to run
    # rubocop:disable Metrics/MethodLength
    def runjob(id, time, options)
      retries ||= 0

      res = Net::HTTP.post(
        URI("#{settings.config['rundeck']['apiprefix']}/job/#{id}/run"),
        {
          'options'   => options,
          'runAtTime' => time
        }.to_json,
        'X-Rundeck-Auth-Token' => settings.config['rundeck']['authtoken'],
        'Content-Type'         => 'application/json'
      )
      log_info "Rundeck API call result: #{res}"
    rescue Net::ReadTimeout => e
      log_error "Timed out while trying to connect #{e} (Retries: #{retries})"
      retry if (retries += 1) < 3
    end
    # rubocop:enable Metrics/MethodLength

    # Schedule the follow-up email for a user
    def schedule_followup(username)
      runjob(settings.config['rundeck']['sendfollowup'], DateTime.now + 7, { 'username' => username })
      log_info("Scheduled follow-up email for #{username}")
    end
  end
  # rubocop:enable Metrics/BlockLength
end
