# frozen_string_literal: true

# Handle interaction with the puppet configuration repository
class Manager2Api < Sinatra::Base
  before do
    git_setup
  end

  # rubocop:disable Metrics/BlockLength
  helpers do
    # Precursor/setup stuff
    # rubocop:disable Metrics/AbcSize
    # rubocop:disable Metrics/MethodLength
    def git_setup
      retries ||= 0
      if Dir.exist? settings.config['repo']['location']
        (Git.open settings.config['repo']['location']).pull
      else
        g = Git.clone(
          settings.config['repo']['uri'],
          settings.config['repo']['name'],
          path: settings.config['repo']['base']
        )
        g.config('user.name', settings.config['repo']['username'])
        g.config('user.email', settings.config['repo']['usermail'])

        log_info 'Used git clone to grab configuration repository'
      end
    rescue Git::GitExecuteError
      log_error 'Git error! retrying...'
      sleep(0.5)
      retry if (retries += 1) < 3
    end
    # rubocop:enable Metrics/MethodLength
    # rubocop:enable Metrics/AbcSize

    # Grab and parse content of hieradata file
    def git_get_full
      YAML.load_file settings.config['repo']['yamlfile']
    end

    def git_get
      git_get_full['profile::lydia::users']
    end

    def git_put(data, message, puppet = true)
      base = git_get_full
      base['profile::lydia::users'] = data.sort.to_h
      git_put_full(base, message, puppet)
    end

    def git_put_full(data, message, _puppet = true)
      File.write settings.config['repo']['yamlfile'], data.to_yaml
      g = Git.open settings.config['repo']['location']
      g.commit_all("#{api_route_name}: #{message}")
      g.push
      log_info message
    end

    # Check if the API key exists
    def git_user_from_key(key)
      data = git_get

      data.each do |user, config|
        next unless config.key?('properties')
        next unless config['properties'].key?('api_key')
        return user if config['properties']['api_key'] == key
      end
      false
    end

    # Check if API key belongs to admin
    def git_key_is_admin(key)
      data = git_get

      data.each_value do |config|
        next unless config.key?('properties')
        next unless config['properties'].key?('api_key')
        next unless config['properties'].key?('api_admin')
        return true if config['properties']['api_key'] == key
      end
      false
    end

    # Check what the next uidmap_offset should be
    def git_get_free_uidmap_offset
      data      = git_get
      offsets   = []
      newoffset = 0

      data.each_value do |config|
        next unless config.key?('properties')
        next unless config['properties'].key?('uidmap_offset')

        offsets.push config['properties']['uidmap_offset']
      end

      newoffset += 1 while offsets.include? newoffset
      newoffset
    end
  end
  # rubocop:enable Metrics/BlockLength
end
