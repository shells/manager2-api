# frozen_string_literal: true

# Helper functions for user requests
class Manager2Api < Sinatra::Base
  # rubocop:disable Metrics/BlockLength
  helpers do
    # Raw data methods
    def requests_get
      requests_put('requests' => []) unless File.exist? settings.config['requests']['yamlfile']
      YAML.load_file settings.config['requests']['yamlfile']
    end

    def requests_put(data)
      File.write settings.config['requests']['yamlfile'], data.to_yaml
    end

    # rubocop:disable Metrics/MethodLength
    # Add a new request
    def request_submit(username, params, type = api_route_name)
      data = requests_get
      data['requests'].push(
        'type'   => type,
        'user'   => username,
        'params' => params
      )
      requests_put data

      # Notify staff of pending request
      notify_irc_admins "New request for #{username}"
      notify_mail_admins(
        "New request from user: #{username} (#{type})",
        (erb :'email/request_notification', locals: {
          username: username,
          type: type,
          params: params
        })
      )

      # Send confirmation mail to user
      notify_mail_user(
        username,
        'Request is pending approval',
        (erb :'email/request_pending', locals: { username: username })
      )

      log_info "user: #{username}"
    end
    # rubocop:enable Metrics/MethodLength

    # List pending requests
    def requests_pending(user)
      data   = requests_get
      result = { 'requests' => [] }
      data['requests'].each do |r|
        result['requests'].push(r) if r['user'] == user
      end
      result
    end

    # Delete a request
    def request_delete(id)
      data = requests_get
      return false if id.negative? || (id > data['requests'].size - 1)

      data['requests'].delete_at(id)
      requests_put data
    end
  end
  # rubocop:enable Metrics/BlockLength
end
